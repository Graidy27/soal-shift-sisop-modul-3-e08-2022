# Soal Shift Modul 3 Kelompok E08

Anggota :
> Januar Evan - 5025201210\
> Graidy Megananda - 5025201188\
> Helmi Taqiyudin - 5025201152

---
## Tabel Konten
- [Soal 1](#nomor-1)
- [Soal 2](#nomor-2)
  - [Soal 2.a](#2a)
  - [Soal 2.b](#2b)
  - [Soal 2.c](#2c)
  - [Soal 2.d](#2d)
  - [Soal 2.e](#2e)
  - [Soal 2.f](#2f)
  - [Soal 2.g](#2g)
- [Soal 3](#nomor-3)


## Nomor 1
> Step by Step
1. Mendownload file music.zip dan quote.zip
Untuk mendapatkan file tersebut, anda bisa memakai fork exec dan menggunakan thread untuk mendownload 2 file sekaligus seperti ini :
    ```c
    void *downloadfile(void *arg){
        pid_t f1, f2;
        pthread_t id = pthread_self();
        if(pthread_equal(id,tid[0])){
            f1 = fork();
            if(f1 == 0){
                pid_t f3;
                f3 = fork();
                int status_f3;
                if(f3 == 0){
                    char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[0], "-O", name[0], NULL};
                    execv("/usr/bin/wget", argv);
                }
                else{
                    while((wait(&status_f3)) > 0);
                    char *argv[] = {"unzip", "-qq", name[0], "-d", namefolder[0], NULL};
                    execv("/usr/bin/unzip", argv);
                }
            }
        }
        else if(pthread_equal(id,tid[1])){
            f2 = fork();
            if(f2 == 0){
                pid_t f4 = fork();
                int status_f4;
                if(f4 == 0){
                    char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[1], "-O", name[1], NULL};
                    execv("/usr/bin/wget", argv);
                }
                else{
                    while((wait(&status_f4)) > 0);
                    char *argv[] = {"unzip", "-qq", name[1], "-d", namefolder[1], NULL};
                    execv("/usr/bin/unzip", argv);
                }
            }
        }
        else if(pthread_equal(id,tid[2])){
            pid_t f5 = fork();
            if(f5 == 0){
                pid_t f6 = fork();
                int status_f6;
                if(f6 == 0){
                    char *argv[] = {"mkdir", "-p",  namefolder[0], NULL};
                    execv("/usr/bin/mkdir", argv);
                }
                else{
                    while((wait(&status_f6)) > 0);
                    pid_t f7 = fork();
                    int status_f7;
                    if(f7 == 0){
                        char *argv[] = {"mkdir", "-p",  namefolder[1], NULL};
                        execv("/usr/bin/mkdir", argv);
                    }
                    else{
                        while((wait(&status_f7)) > 0);
                        char *argv[] = {"mkdir", "-p",  namefolder[2], NULL};
                        execv("/usr/bin/mkdir", argv);
                    }
                }
            }
        }
    }
    ```
    dan juga membuat thread dan pthread_join dalam fungsi main
    ```c
    for(int i = 0; i < 3; i++){
        err = pthread_create(&(tid[i]), NULL, &downloadfile, NULL);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);
    ```
2. Decode file
    Decode file bisa menggunakan website [berikut](https://www.mycplus.com/source-code/c-source-code/base64-encode-decode/)
    Hal yang harus dilakukan adalah mendapatkan nama-nama file dari dalam folder ./music untuk membuka file dan mendapatkan isi dari file tersebut kemudian didecode. Hal ini bisa dilakukan dengan folder ./quote dengan menggunakan thread. Setelah didecode, hasilnya dimasukkan dalam .txt (folder music berarti 'music.txt' dan folder quote 'quote.txt' dan kedua .txt dipindah ke folder hasil(dibuat terlebih dahulu))
    ```c
    void *decode(void *arg){
    pid_t f1, f2;
    pthread_t id = pthread_self();
    if(pthread_equal(id,tid[0])){
        DIR *d1;
        struct dirent *dir1;
        d1 = opendir("/home/januarevan/modul3/music");
        while ((dir1 = readdir(d1)) != NULL) {
            if(strstr(dir1->d_name, ".txt") != NULL){
                strcpy(musicname[k1], dir1->d_name);
                k1++;
            }
        }
        closedir(d1);
        char tempdir[FILENAME_MAX];
        char buffer[1024]; 
        FILE *fp2 = fopen("/home/januarevan/modul3/hasil/music.txt", "a");
        for(int i = 0; i < k2; i++){
            memset(buffer, 0, sizeof(buffer));
            strcpy(tempdir, default_path);
            strcat(tempdir, "/music/");
            strcat(tempdir, musicname[i]);
            FILE *fp1 = fopen(tempdir, "r");
            fgets(buffer, 1024, fp1);
            size_t decode_size = strlen(buffer);
            char *decoded = malloc(sizeof(char)*32768);
            decoded = base64_decode(buffer, decode_size, &decode_size);
            fprintf(fp2, "%s\n", decoded);
            fclose(fp1);
        }
        fclose(fp2);
    }
    else if(pthread_equal(id,tid[1])){
        DIR *d2;
        struct dirent *dir2;
        d2 = opendir("/home/januarevan/modul3/quote");
        while ((dir2 = readdir(d2)) != NULL) {
            if(strstr(dir2->d_name, ".txt") != NULL){
                strcpy(quotename[k2], dir2->d_name);
                k2++;
            }
        }
    ```
    dan juga jangan lupa membuat thread dan pthread_join
    ```c
    for(int i = 0; i < 2; i++){
        err = pthread_create(&(tid[i]), NULL, &decode, NULL);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    ```
3. Pindahkan kedua file .txt ke folder hasil
    Untuk ini, hanya menggunakan fork exec zip (dikasih password mihinomenest"nama-anda")
    ```c
    void *zip(void *arg){
        pid_t f1, f2;
        pthread_t id = pthread_self();
        if(pthread_equal(id,tid[0])){
            f1 = fork();
            if(f1 == 0){
                char *argv[]={"zip", "-P", "mihinomenestjanuarevan", "-r", "hasil.zip", "hasil", NULL};
                execv("/usr/bin/zip",argv);
            }
        }
    }
    ```
    jangan lupa buat thread dan pthread_join
    ```c
    for(int i = 0; i < 1; i++){
        err = pthread_create(&(tid[i]), NULL, &zip, NULL);
    }
    pthread_join(tid[0], NULL);
    ```
4. menambahkan file No.txt dan zip kembali
    untuk hal ini, harus dihapus folder hasil, bersamaan unzip hasil.zip, bersamaan juga membuat file no.txt dengan isi No
    kemudian dizip kembali dengan folder hasil dan juga no.txt
    ```c
    void *unzipadd(void *arg){
        pid_t f1, f2;
        pthread_t id = pthread_self();
        if(pthread_equal(id,tid[0])){
            f1 = fork();
            if(f1 == 0){
                char *argv[]={"rm","-r","hasil", NULL};
                execv("/usr/bin/rm",argv);
            }
        }
        else if(pthread_equal(id,tid[1])){
            f2 = fork();
            if(f2 == 0){
                usleep(1000);
                char *argv[] = {"unzip", "-P", "mihinomenestjanuarevan", "-qq", name[2], NULL};
                execv("/usr/bin/unzip", argv);
            }
        }
        else if(pthread_equal(id,tid[2])){
            FILE *fp1 = fopen("/home/januarevan/modul3/no.txt", "a");
            fprintf(fp1,"No");
            fclose(fp1);
        }
    }
    void *zip2(void *arg){
        pid_t f1, f2;
        pthread_t id = pthread_self();
        if(pthread_equal(id,tid[0])){
            f1 = fork();
            if(f1 == 0){
                char *argv[]={"zip","-P","mihinomenestjanuarevan", "-r", "hasil.zip", "hasil", "no.txt", NULL};
                execv("/usr/bin/zip",argv);
            }
        }
        else if(pthread_equal(id,tid[0])){
            f2 = fork();
            if(f2 == 0){
                char *argv[]={"rm","-r","hasil.zip", NULL};
                execv("/usr/bin/rm",argv);
            }
        }
    }
    ```
    dan juga membuat thread dan pthread_join
    ```
    for(int i = 0; i < 3; i++){
        err = pthread_create(&(tid[i]), NULL, &unzipadd, NULL);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);
    sleep(5);
    for(int i = 0; i < 1; i++){
        err = pthread_create(&(tid[i]), NULL, &zip2, NULL);
    }
    pthread_join(tid[0], NULL);
    ```
> Permasalahan yang dihadapi
1. Tidak bisa membuat 2 lebih thread (lupa pakai pthread_join)
2. Decode base_64 masih kurang sesuai (masalah pada memory allocation)
![](pic/image__14_.png)

## Nomor 2

**[Source Code Client](https://gitlab.com/Graidy27/soal-shift-sisop-modul-3-e08-2022/-/blob/main/soal2/Client/client.c)**\
**[Source Code Soal Server](https://gitlab.com/Graidy27/soal-shift-sisop-modul-3-e08-2022/-/blob/main/soal2/Server/server.c)**

### 2.a
#### Deskripsi
Membuat client memiliki 2 opsi, yaitu login dan register
#### Pembahasan
##### Client
- Fungsi `dua_a`\
Kode berikut digunakan untuk menjawab soal 2g
```
	puts("You will access the server when no one is currently accessing it\nPlease wait...\n");
	sleep(3);
	char permission[1024] = {0};
	read(sock, permission, 1024);
	puts("Welcome!"); sleep(2);
```


Pertama, kita memberitahu user bahwa ada 2 pilihan, yaitu login dan register. Kemudian, pilihan user dikirim kepada server
```
	printf("Please input 0 or 1\n");
	printf("0 : Login\n1 : Register\n");
	char option[2]; in(option);
	write(sock, option, 2);
```

Kemudian, sistem akan meminta user untuk memasukkan username dan input. 
Username harus mengandung huruf besar atau kecil. Untuk mengecek hal tersebut, digunakan fungsi `in`. Jika input sudah benar, maka akan dikirim ke server menggunakan `write`.
```
	char username[1024], password[1024];
	while(true){
		invalid = 0;
		printf("Username : ");
		in(username);
		if(besar == 0 && kecil == 0) {printf("Username must cointain either uppercase or lowercase\n"); continue;}
		if(!invalid) {write(sock, username, 1024); break;}
		else {printf("Invalid character\n"); continue;}
	}
```

Password harus terdiri dari 6 karakter yang terdiri dari huruf besar, huruf kecil, dan angka. Jika input sudah benar, maka akan dikirim ke server menggunakan `write`. Variabel global `invalid`, `besar`, `kecil`, dan `angka` diinisiasi sama dengan 0.
```
	while(true){
		invalid = 0; besar = 0; kecil = 0; angka = 0;
		printf("Password : ");
		in(password);
		if(besar == 0 || kecil == 0 || angka == 0) {printf("Please use upper case, lower case, AND number\n"); continue;}
		if(!invalid) {
			if(strlen(password) > 5) {write(sock, password, 1024); break;}
			else printf("Must contain at least 6 character\n");
		}
		else printf("Invalid character\n");
	}
```

Respon dari server akan disimpan ke dalam variabel status dan ditampilkan. Untuk lebih lanjut, dapat dilihat pada pembahasan fungsi `dua_a` di server.
```
	char status[200] = {0};
	read(sock, status, 200);
	printf("%s\n", status);
```

Jika respon dari server adalah "USERNAME EXIST" atau "USERNAME REGISTERED" atau "ACCESS DENIED" maka user dapat memilih untuk kembali ke main menu (untuk register atau login) atau keluar dari sistem.
```
	if(strcmp(status, "USERNAME EXIST") == 0 || strcmp(status, "USERNAME REGISTERED\0") == 0 || strcmp(status, "ACCESS DENIED") == 0) {
		printf("Back to main menu? (Y/N)\n");
		char c[2]; scanf("%c", &c[0]);
		while(c[0] != 'Y' && c[0] != 'N') {if(c[0] != '\n') printf("Wrong Input. Try again\n"); scanf("%c", &c[0]);}
		write(sock, c, 2);
		if(c[0] == 'Y') return dua_a(sock);
		return 0;
	}
```

Jika respon dari user adalah "ACCESS GRANTED", maka fungsi akan mengembalikan nilai 1. Hal ini dikarenankan untuk mengakses fitur yang harus dibuat untuk menjawab soal b hingga f, user harus login menggunakan username dan password yang tersedia pada `users.txt`.
```
	return 1;
```

- Fungsi `in`\
Idenya adalah membaca suatu string perkarakter dengan menggunakan while loop. Variabel global `besar`, `kecil`, dan `invalid` dideklarasikan secara global (**[Lihat source Code Client](https://gitlab.com/Graidy27/soal-shift-sisop-modul-3-e08-2022/-/blob/main/soal2/Client/client.c)**).\
Jika terdapat huruf kecil, maka nilai variabel `kecil` akan sama dengan 1.\
Jika terdapat huruf besar, maka nilai variabel `besar` akan sama dengan 1.\
Jika terdapat angka, maka nilai variabel `angka` akan sama dengan 1.\
Jika terdapat selain itu, maka nilai variabel `invalid` akan sama dengan 1.\
Variabel `i` digunakan untuk menandkan karakter ke-i. Loop akan berhenti ketika menemukan karakter new line dan `i` tidak sama dengan 0.
```
	int i = 0;
	while(true){
		scanf("%c", &*(var+i));
		if(*(var+i) <= 90 && *(var+i) >= 65) {
			i++;
			besar = 1;
		}
		else if(*(var+i) <= 122 && *(var+i) >= 97) {
			i++;
			kecil = 1;
		}
		else if(*(var+i) <= 57 && *(var+i) >= 48) {
			i++;
			angka = 1;
		}
		else if(*(var+i) == '\n' && i != 0) {
			*(var+i) = '\0';
			return;
		}
		else if(*(var+i) == '\n' && i == 0) continue;
		else invalid = 1;
	}
```


##### Server
- Fungsi `dua_a`\
Kode berikut digunakan untuk menjawab soal 2g
```
	write(new_socket, "Go", 1024);
```

Pilihan dari user, username, dan password akan disimpan berturut-turut ke dalam variabel `opsi`, `username`, dan `password`.
```
	char option[2] = {0}, username[1024] = {0}, password[1024] = {0};
	read(new_socket, option, 2); 		//puts(option);
	read(new_socket, username, 1024);	//puts(username);
	read(new_socket, password, 1024);	//puts(password);
```

Karena baik opsi login maupun register membutuhkan sistem untuk mengakses `users.txt`, maka dibuatlah variabel pointer to file `fp`.
```
	FILE* fp;
```

Kemudian, path file `users.txt` disimpan ke dalam variabel `temp_path` dengan menggunakan `strcat`.
```
	char temp_path[1024] = {0};
	strcat(temp_path, path);
	strcat(temp_path, "/Server/users.txt");
```

File dibuka dengan mode append (untuk register) dan read (untuk login) 
```
	fp = fopen(temp_path, "a+");
```

Username dan password kemudian digabung dengan menggunakan format `username:password` sesuai perintah soal dan disimpan ke dalam variabel `temp`. 
```
	char temp[1024] = {0}, db[1024] = {0}, login[2] = {0};
	int correct_pass = 0, user_uniq = 1;
	strcat(temp, username);
	strcat(temp, ":");
	strcat(temp, password);
```

Seluruh isi file akan dibaca secara bertahap (per baris) dengan menggunakan `while(fgets())`. Baris yang dibaca akan disimpan ke dalam variabel `db`.  
```
	while(fgets(db, sizeof(db), fp)){
		...
	}
```

Variabel `total_user` merupakan variabel global yang menyimpan banyaknya user yang terdaftar.
Karena karakter new line juga terbaca, maka karakter tersebut harus dihilangkan.
```
		total_user++;
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';
```

Jika user memilih login, maka bandingkan variabel temp dan db menggunakan `strcmp`. Jika me return nilai 0, maka artinya baik username maupun passwordnya sudah benar. Karena itu, variabel `correct_pass` diberikan nilai 1 dan keluar dari loop. Jika sampai file `users.txt` habis terbaca hasil `strcmp` tidak sama dengan 0, maka `correct_pass` akan tetap bernilai 0.`continue;` digunakan agar tidak perlu menjalankan code yang berada di bawahnya. Karena code tersebut digunakan jika user memilih register.
```
		if(option[0] == '0') {
			if(!strcmp(temp, db)) {
				correct_pass = 1;
				break;
			}
			continue;
		}
```

Jika user memilih register, maka kode berikut akan dijalankan. Variabel `i` digunakan untuk menyimpan banyaknya character yang sudah dibandingkan. Hal ini untuk mencegah akses memory di luar string yang akan dibandingkan. Perbandingan karakter akan dilakukan sampai terbaca `:`. Hal ini dikarenakan karakter setelah `:` merupakan password. Jika selalu sama, maka `user_uniq` akan tetap bernilai 0 yang artinya usernamenya sudah ada. Jika ada yang berbeda, maka loop akan berhenti dan `user_uniq` tetap bernilai 1 yang berarti usernamenya belum terpakai.
```
		int i = 0;
		while(i < strlen(temp) && i < strlen(db)){
			if(db[i] != temp[i]) break;
			if(db[i] == ':') user_uniq = 0;
			i++;
		}
		if(user_uniq == 0) break;
```

Setelah keluar dari loop, maka akan dikirimkan pesan kepada client berdasarkan opsi yang client pilih yang tersipan di dalam variabel `option`. 
Jika opsinya 0, maka akan mengirim `ACCESS GRANTED ` jika `correct_pass` bernilai 1 (artinya username dan passwordnya benar) dan "ACCESS DENIED" jika sebaliknya.  
```
	char status[200] = {0};
	if(option[0] == '0') {
//		printf("Login\n");
		if(correct_pass) {
			strcpy(status, "ACCESS GRANTED");
			strcpy(login_user, username);
		}
		else strcpy(status, "ACCESS DENIED");
	}
```

Jika opsinya 1, maka akan mengirim "USERNAME EXIST" jika `user_uniq` (username sudah digunakan) bernilai 0 dan "USERNAME REGISTERED" jika sebaliknya.
```
	else if(option[0] == '1') {
//		printf("Register\n");
		if(!user_uniq) strcpy(status, "USERNAME EXIST");
		else if(user_uniq){
			if(total_user >0) fprintf(fp, "\n");
			fprintf(fp, "%s", temp);
			strcpy(status, "USERNAME REGISTERED");
		}
	}
```

Selanjutnya mengirim `status` ke client dan menutup file menggunakan `fclose`.
```
	write(new_socket, status, 200);
	char back_to_main_menu[2] = {0};
	read(new_socket, back_to_main_menu, 2);
//	printf("%c\n", back_to_main_menu[0]);
	fclose(fp);
```

Karena untuk mengakses fitur yang harus dibuat untuk menjawab soal b hingga f user harus login, maka jika user berhasil login fungsi `dua_a` akan mengembalikan nilai 1. 
Jika tidak, maka perlu dipastikan apakah user ingin kembali ke main menu. Jika iya, fungsi `dua_a` dipanggil kembali. Jika sebaliknya, maka akan mengembalikan nilai 0. 
```
	if(strcmp(status, "ACCESS GRANTED") == 0) return 1;
	if(back_to_main_menu[0] == 'Y') return dua_a(new_socket);
	return 0;

```

### 2.b
#### Deskripsi
Membuat tab separated file (tsv) pada saat server dijalankan. File itu berisi daftar judul dan author problem yang ada.

#### Pembahasan
Catatan: Beberapa komen dihapus. Berhubung file tsv berada pada Server, maka kodenya di tulis di server.

##### Server
- Fungsi `dua_b`\
Variabel `path` merupakan variabel global dan menyimpan path ke `soal2`. Untuk lebih jelasnya, **[Lihat di sini ](https://gitlab.com/Graidy27/soal-shift-sisop-modul-3-e08-2022/-/blob/main/soal2/Server/server.c)**.\
Variabel global `tsv_path` menyimpan path yang akan digunakan untuk menyimpan file tsv dengan menggunakan `strpy` (copy string di `path` ke `tsv_path`) dan `strcat`.\
Pinter to file `fp` digunakan untuk membaca, jika problem.tsv sudah dibuat, atau mengappend, jika belum dibuat. Jika belum ada, maka line pertama jadi tsv berisi Jusul probem dan Author Problem.
```
void dua_b(){
	strcpy(tsv_path, path);				
	strcat(tsv_path, "/Server/problem.tsv");

	FILE* fp;
	if(fp = fopen(tsv_path, "r")) {printf("Exist\n");fclose(fp);}
	else{
		printf("File doesn't exist\n");
		fp = fopen(tsv_path, "a");
		fprintf(fp, "Judul Problem\tAuthor Problem\n");
		fclose(fp);
	}
}
```

### 2.c
#### Deskripsi
Membuat fitur `add`. Yaitu menambah soal ke dalam data base (file tsv) dan membuat folder berisi deskripsi, input, dan output soal di dalam folder.\
Folder tersebut berada di dalam folder server dan namanya sesuai nama soal.

#### Pembahasan
##### Client
- Fugnsi `dua_c`\
Variabel `q` dan `a` diinisiasi. Kedua variabel tersebut akan digunakan untuk menyimpan string yang diterima dari server. While loop digunakan untuk mengirim judul soal, path deskripsi, path input, dan path output ke server. While akan berhenti jika panjang string yang diterima = 0.
```
	//printf("dua c\n");
	char q[1024] = {0}, a[1024] = {0};
	read(sock, q, 1024); printf("%s", q);
	read(sock, q, 1024);
	while(strlen(q)) {
		printf("%s", q);
		scanf("%s", a);
		write(sock, a, 1024);
		read(sock, q, 1024);
	}
```

##### Server
- Fungsi `dua_c`\
Pertama, server mengirim petunjuk agar user tidak bingung.
```
	//Notice
	write(new_socket, "\nIf the problem name already exist, the program will stop immediately.\nProblem name, input, and output will\nbe updated after user exit the program\n\n", 1024);
	char a[1024] = {0}, apn[1024] = {0}, temp_path[1024] = {0};
```

Selanjutnya, server meminta judul problem. Jika judul problem sudah ada, maka server akan mengirim NULL character. Jika problem tidak ada, maka folder dengan nama judul problem akan dibuat.
```
	write(new_socket, "Judul problem: ", 1024); 		read(new_socket, a, 1024);
	take_mid_only(a); //puts(a);
	//folder
	if(mkdir(a, S_IRWXU)) {write(new_socket, "\0", 1024); return;}
```

Karena jumlah problem di server bertambah, maka database diupdate.
```
	//upate tsv
	FILE *fp = fopen(tsv_path, "a");
	fprintf(fp, "%s\t%s\n", a, login_user);
	fclose(fp);
```

Variabel `temp_path` akan berisi path untuk menyimpan deskripsi, input, dan output problem.
```
	strcpy(temp_path, path);
	strcat(temp_path, "Server/");
	strcat(temp_path, a);
```

Server meminta path yang berisi deskripsi soal.\
Karena format yang diminta soal, maka digunakan fungsi `take_mid_only` untuk mengambil string diantara tanda kurang dan lebih dari.\
Kemudian, server akan membuat file dengan nama `description.txt`. Isi dari deskripsi pada client akan di salin ke `description.txt` dengan menggunakan fungsi `copy_file`.
```
	//description
	write(new_socket, "Filepath description.txt: ", 1024);	read(new_socket, a, 1024); //puts(a);
	take_mid_only(a); //puts(a);
	copy_file(temp_path, a, "/description.txt");
```

Server meminta path yang berisi input soal.\
Karena format yang diminta soal, maka digunakan fungsi `take_mid_only` untuk mengambil string diantara tanda kurang dan lebih dari.\
Kemudian, server akan membuat file dengan nama `input.txt`. Isi dari input pada client akan di salin ke `input.txt` dengan menggunakan fungsi `copy_file`.
```
	//input
	write(new_socket, "Filepath input.txt: ", 1024);	read(new_socket, a, 1024); //puts(a);
	take_mid_only(a); //puts(a);
	copy_file(temp_path, a, "/input.txt");
```

Server meminta path yang berisi output soal.\
Karena format yang diminta soal, maka digunakan fungsi `take_mid_only` untuk mengambil string diantara tanda kurang dan lebih dari.\
Kemudian, server akan membuat file dengan nama `output.txt`. Isi dari output pada client akan di salin ke `output.txt` dengan menggunakan fungsi `copy_file`.
```
	//output
	write(new_socket, "Filepath output.txt: ", 1024);	read(new_socket, a, 1024); //puts(a);
	take_mid_only(a); //puts(a);
	copy_file(temp_path, a, "/output.txt");
```

Mengirim karakter null agar loop di client berhenti.
```
	//stop input
	write(new_socket, "\0", 1024);
```

- Fungsi `take_mid_only`\
Variabel `i` akan digunakan untuk menunjuk pointer to char.\
Karena karakter di index 0 pasti tanda kurang dari, maka nilai `i` diinisiasi 1.\
Variabel `temp` digunakan untuk menyimpan karakter yang berada di antara tanda kurang dan lebih dari.\
Jika pointer to char sudah menunjuk ke alamat yang berisi `>`, maka loop akan berhenti dan isi dari `temp` disimpan ke dalam `a`.
```
void take_mid_only(char *a){
	char temp[100] = {0};
	int i = 1;
	while(*(a + i) != '>'){
		temp[i-1] = *(a + i);
		i++;
	}
	strcpy(a, temp);
}
```

- Fungsi `copy_file`\
Pointer to file `tujuan` berisi path ke folder tujuan\
Pointer to file `path_asal` berisi path ke file teks asal\
Pointer to file `fp_asal` digunakan untuk membaca path file teks asal\
Pointer to file `fp_tujuan` digunakan untuk membuat dan menulis file teks tujuan\
Loop digunakan untuk menulis isi dari file teks asal ke file teks tujuan.
```
void copy_file(char *tujuan, char *path_asal, char file_type[100]){
	FILE *fp_asal = fopen(path_asal, "r");

	char path_tujuan[1024] = {0};
	strcpy(path_tujuan, tujuan);
	strcat(path_tujuan, file_type);
	FILE *fp_tujuan = fopen(path_tujuan, "a");
	char temp[1024] = {0};
//	printf("tujuan : %s\nasal: %s\n\n", path_tujuan, path_asal);
	while(fgets(temp, sizeof(temp), fp_asal)) fprintf(fp_tujuan, "%s", temp);
	fclose(fp_asal);
	fclose(fp_tujuan);
}
```
### 2.d
#### Deskripsi
Membuat fitur `see`, yaitu menampilkan judul dan author soal yang ada di database.

#### Pembahasan
##### Client
- Fungsi `dua_d`\
Varibael `message` digunakan untuk menyimpan pesan dari server.\
Selama panjang pesan tidak sama dengan 0, maka itu artinya server mengirim judul dan author problem (dalam format yang sudah ditentukan, yaitu diantara tanda kurang dan lebih dari). Karena itu, isinya akan ditampilkan\
```
void dua_d(int sock){
//	printf("dua d\n");
	char message[1024] = {0};

	int spasi = 1, i = 2;
	read(sock, message, 1024);
//	printf("T: %s\n", message);
	while(strlen(message)){
		printf("<%s>", message);
		if(spasi) {printf(" "); spasi = 0;}
		else {printf("\n"); spasi = 1;}
		read(sock, message, 1024);
	}
	puts("");
}
```

##### Server
- Fungsi `dua_d`\
Pointer to file `fp` digunakan untuk membaca database.\
Jika file dapat dibuka, maka isinya dapat dikirim ke client.
```
printf("2d\n");
        FILE* fp; int judul = 1;
	if(fp = fopen(tsv_path, "r")) {
		...
	}
```

Didalam if, akan ditulis kode berikut.
Variabel `temp` digunakan untuk menyimpan setiap baris pada database sampai seluruh baris sudah terbaca. Untuk setiap line, akan dilakukan beberapa hal (kode di dalam loop).
```
//berikut merupakan kode di dalam if(fp = fopen(tsv_path, "r"))
		char temp[1024] = {0};
		//mengirim semua data soal
		while(fgets(temp, sizeof(temp), fp)) {
			...
		}
```

Variabel `judul` digunakan untuk melewatkan baris pertama di database dalam iterasi, karena itu bukan merupakan informasi.\
Variabel `temp2` digunakan untuk menyimpan judul dan author soal.\
Variabel `i` digunakan untuk membantu akses ke setiap index di variabel `temp`.\
Varibael `j` digunakan untuk membantu akses ke setiap index di variabel `temp2`.\
Loop kedua (`while(1)`) digunakan untuk membaca string di variabel `temp` per karakter.\
Karakter di dalam `temp` akan terus dibaca dan disimpan ke dalam `temp2` hingga ditemukan karakter tab. Itu menandakan bahwa judul sudah terbaca semua, sehingga isi string `temp2` dikirim ke client dan nilai variabel `j` menjadi 0.\
Selanjutnya dilakukan hal yang sama hingga ditemukan karakter new line. Hal ini menandakan bahwa nama author telah terbaca seluruhnya. Karena itu, isi string `temp2` dikirim ke client dan loop berhenti.
```
//berikut merupkan kode didalam while(fgets(temp, sizeof(temp), fp))
			if(judul) {judul = 0; continue;}
			char temp2[1024] = {0}; int i = -1, j = 0;
			while(1){
				i++;
				if(temp[i] == '\t') {
					temp2[j] = '\0';
//					printf("judul: %s\n", temp2);
					write(new_socket, temp2, 1024);
					j = 0;
					continue;
				}
				else if(temp[i] == '\n'){
					temp2[j] = '\0';
					//printf("author: %s\n", temp2);
					write(new_socket, temp2, 1024);
					break;
				}
				temp2[j] = temp[i];
				j++;
			}
```


Terakhir, server mengirim null character agar looop di client berhenti.
```
		write(new_socket, "\0", 10);
		fclose(fp);
```

### 2.e
#### Deskripsi
Membuat fitur `download` untuk mendownload soal dari server ke client.

#### Pembahasan
##### Client
- Fungsi `dua_e`\
Varibael `message` digunakan untuk menyimpan string yang dikirim dari server.\
Jika string pertama yang dikirim adalah null character, maka problemnya tidak ada. Oleh karena itu, tidak diperlukan untuk menjalankan kode yang berada di bawahnya\
Kemudian, path di directory client saat ini akan dikirim ke server.\
Terakhir, client akan kembali membaca string yang dikirim oleh server.

```
void dua_e(int sock){
	char message[1024] = {0}; scanf("%s", message);
	write(sock, message, 1024);
	read(sock, message, 1024);

	//check problem availability
	if(!strlen(message)) {printf("Problem name not availabel\n\n"); return;}

	//mkdir
	mkdir(message, S_IRWXU);

	//send current working directory
	getcwd(message, 1024); write(sock, message, 1024);

	//get notice
	read(sock, message, 1024);
	printf("%s", message);
	sleep(2);
}
```

##### Server
- Fungsi `dua_e`\
Idenya mirip dengan `add`, yaitu dengan menggunakan fungsi `copy_file` dan `take_mid_only`. Namun, jika pada `add` yang dijadikan asal ada client dan yang dijadikan tujuan adalah server, kini yang dijadikan asal adalah server dan yang dijadikan tujuan adalah client.
```
void dua_e(int new_socket){
	char message[1024] = {0};
	read(new_socket, message, 1024);
	take_mid_only(message);
	if(mkdir(message, S_IRWXU)) {
		write(new_socket, message, 1024);
		char c_working_dir[1024] = {0}, problem_dir[1024] = {0};
		getcwd(problem_dir, 1024);
		strcat(problem_dir, "/");
		strcat(problem_dir, message);
		strcat(problem_dir, "/");

		int cd = strlen(problem_dir);
		read(new_socket, c_working_dir, 1024);
		strcat(c_working_dir, "/");
		strcat(c_working_dir, message);

		//description
		strcat(problem_dir, "description.txt");
		copy_file(c_working_dir, problem_dir, "/description.txt");
		problem_dir[cd] = '\0';

		//input
		strcat(problem_dir, "input.txt");
		copy_file(c_working_dir, problem_dir, "/input.txt");
		problem_dir[cd] = '\0';

		//output
		strcat(problem_dir, "output.txt");
		copy_file(c_working_dir, problem_dir, "/output.txt");

		write(new_socket, "Problem name, input, and output will\nbe updated after user exit the program\n\n", 1024);
	}
	else {rmdir(message); write(new_socket, "\0", 1024);}
}
```

### 2.f
#### Deskripsi
Membuat fitur `submit` untuk membandingkan file output yang client kirim dan yang berada pada server.\
Jika sama maka kirim `AC`.\
Jika tidak sama, maka kirim `WA`

#### Pembahasan
##### Client
- Fungsi `dua_f`\
Variabel `judul` akan digunakan untuk menyimpan judul dari soal yang ingin dibandingkan output.txtnya.\
Variabel `ans_path` berisi path dari file output.txt yang ingin client bandingkan.\
Pertama, simpan judul dan path jawaban berturut-turut ke dalam variabel `judul` dan `ans_path` lalu kirim keduanya ke server.\
Jika server mengirim null character, maka artinya problem tersebut tidak tersedia.\
Jika sebaliknya, maka server selanjutnya akan mengirim `WA` atau `AC`
```
void dua_f(int sock){
	char judul[1024] = {0}, ans_path[1024] = {0};
	scanf("%s%s", judul, ans_path);
	write(sock, judul, 1024);
	write(sock, ans_path, 1024);

	char status[1024] = {0};
	read(sock, status, 1024);

	//check problem availability
	if(!strlen(status)) {printf("Problem name not availabel\n\n"); return;}

	//check status
	while(strlen(status)) {
		printf("%s\n", status); sleep(2);
		read(sock, status, 1024);
	}
}
```

##### Server
- Fungsi `dua_f`\
Variabel `judul` akan digunakan untuk menyimpan judul dari soal yang ingin dibandingkan output.txtnya.\
Variabel `ans_path` berisi path dari file output.txt yang ingin client bandingkan.\
Pertama, simpan judul dan path jawaban berturut-turut ke dalam variabel `judul` dan `ans_path`.\
Seperti soal-soal sebelumnya, digunakan pointer to file untuk membuka kedua output.txt di client dan di server untuk dibandingkan.\
Dengan menggunakan `while(a == 1 || p == 1)`, setiap kedua output.txt akan setiap barisnya.\
Jika seluruh baris sama, maka kirimkan `AC` ke client.\
Jika salah satu file output habis terbaca atau salah satu barisnya tidak sama, maka kirimkan `WA` ke client.\
```
//NOMOR 2f
void dua_f(int new_socket){
	char judul[1024] = {0}, ans_path[1024] = {0};
	read(new_socket, judul, 1024); take_mid_only(judul);
	read(new_socket, ans_path, 1024); take_mid_only(ans_path);
//	printf("judul: %s\nans_path: %s\n", judul, ans_path);
	if(mkdir(judul, S_IRWXU)) {
		//set status
		write(new_socket, "Problem found\n", 1024);
		char problem_dir[1024] = {0};
		getcwd(problem_dir, 1024);
		strcat(problem_dir, "/");
		strcat(problem_dir, judul);
		strcat(problem_dir, "/output.txt");
//		printf("ans: %s\nprob: %s\n", ans_path, problem_dir);
		FILE *fp_prob = fopen(problem_dir, "r");
		FILE *fp_ans = fopen(ans_path, "r");
		char temp[1024] = {0}, temp2[1024] = {0};
		int a = 1, p = 1, same = 0;

		while(a == 1 || p == 1) {
			if(strcmp(temp, temp2)) break;
			if(!fgets(temp, sizeof(temp), fp_prob)) p = 0;
			if(!fgets(temp2, sizeof(temp2), fp_ans)) a = 0;
			if(a == 0 && p == 0) same = 1;
		}
		if(same) write(new_socket, "AC", 1024);
		else write(new_socket, "WA", 1024);
		fclose(fp_ans);
		fclose(fp_prob);
		write(new_socket, "\0", 1024);
	}
	else {rmdir(judul); write(new_socket, "\0", 1024);}
}
```

### 2.g
#### Deskripsi
Membuat server dapat mengantrikan client yang ingin mengakses server. Dalam satu waktu, hanya boleh ada 1 client yang mengakses server. Sisanya diharuskan mengunggu.

#### Pembahasan
##### Server
- Di fungsi `main`\
Jika biasanya digunakan if(), maka sekarang digunakan while dan thread agar server dapat terus menyala kecuali di terminate.\
Thread yang dijalankan adalah `connection_control` (atau dapat dibilang bagian main menu)
```
	while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))) {
		printf("Connected to client\n");
		pthread_t client_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		if(pthread_create(&client_thread, NULL, connection_control, (void*) new_sock) < 0) {
			perror("Could not create thread");
			return 1;
		}
		pthread_join(client_thread, NULL);
		puts("Disconnected");
		puts("Waiting for client");
	}
```

- Fungsi pointer `connection_control` (ini untuk thread)\
server mengirim pesan `go` untuk menandakan server sudah dapat diakses.\
Jika pada fungsi `dua_a` server berhasil login, maka fitur-fitur yang ada dapat digunakan.\
Client dapat keluar jika menginputkan `exit`.
```
void *connection_control(void *new_sock){
	int new_socket = *(int*) new_sock;
	dua_b();
	write(new_socket, "Go", 1024);
	if(dua_a(new_socket)) {
		printf("Client Login\n");
		while(1){
			char com[1024] = {0};
			read(new_socket, com, 1024);
//			printf("masok while\n");
			if(!strcmp(com, "add")) dua_c(new_socket);
			else if(!strcmp(com, "see")) dua_d(new_socket);
			else if(!strcmp(com, "download")) dua_e(new_socket);
			else if(!strcmp(com, "submit")) dua_f(new_socket);
			else if(!strcmp(com, "exit")) {printf("See you later!\n"); break;}
		}
	}
}
```
### Permasalahan yang dihadapi
1. Lupa membuat error pada register (2a) jika password terdiri kurang dari 6 karakter
2. Waktu pengerjaan hanya keburu untuk mengerjakan sampai 2c

## Nomor 3
Nami adalah seorang pengoleksi harta karun handal. Karena Nami memiliki waktu luang, Nami pun mencoba merapikan harta karun yang dimilikinya berdasarkan jenis/tipe/kategori/ekstensi harta karunnya. Setelah harta karunnya berhasil dikategorikan, Nami pun mengirimkan harta karun tersebut ke kampung halamannya.

### 3.a, 3.b, 3.c
a. Hal pertama yang perlu dilakukan oleh Nami adalah mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”. Kemudian working directory program akan berada pada folder “/home/[user]/shift3/hartakarun/”. Karena Nami tidak ingin ada file yang tertinggal, program harus mengkategorikan seluruh file pada working directory secara rekursif

b. Semua file harus berada di dalam folder, jika terdapat file yang tidak memiliki ekstensi, file disimpan dalam folder “Unknown”. Jika file hidden, masuk folder “Hidden”.

c. Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread.

```c
void listfilesrecursively(const char *name,int indent)
{
    DIR *dir;
    struct dirent *entry;
    int err;
    if (!(dir = opendir(name)))
    {
        printf("Cannot open directory '%s'\n", name);
        return;
    }
    while ((entry = readdir(dir)) != NULL) 
    {
        char path[1024];
        if (entry->d_type == DT_DIR) 
        {
            args *arg = malloc(sizeof(args));
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            listfilesrecursively(path, indent + 2); 
        }
        else
        {
            char cwd[100];
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            args *arg = malloc(sizeof(args));
            strcpy(arg->arg1,path);
            strcpy(arg->arg2,entry->d_name);
            err = pthread_create(&tid[pthreadcount++], NULL, &categorize, (void *)arg);
            if(err)
            {
                printf("\ncan't create thread :[%s]",strerror(err));
                continue;
            }
        }
    }
    closedir(dir);
}
```
kode diatas digunakan untuk memproses file file secara rekursif yang kemudian akan dilanjutkan ke fungsi `categorize`

```c
void *categorize(void *arg)
{
    args *a = (args *)arg;
    char *path = a->arg1;
    char *src= a->arg2;
    char temp[200];
    char lowerext[100]; 

    if(src[0] == '.')
    {
        mkdir("Hidden",0777);
        sprintf(temp,"Hidden/%s",src);
        rename(path,temp);
    }
    else if(!getformat(src))
    {
        mkdir("Unknown",0777);
        sprintf(temp,"Unknown/%s",src);
        rename(path,temp);
    }
    else
    {
        char lowerext[50];
        strcpy(lowerext,getformat(src));
        lowercase(lowerext);
        mkdir(lowerext,0777);
        sprintf(temp,"%s/%s",lowerext,src);
        rename(path,temp);
    }
    memset(lowerext,0,sizeof(lowerext));
    memset(temp,0,sizeof(temp));
}
```
pada fungsi `categorize` kita membagi file berdasarkan formatnya. di dalam kode tersebut yang pertama kita cari untuk file hidden, yaitu file yang diawali oleh karakter `.`, kemudian dibuat folder kategorinya dengan `mkdir` dan memindahkan file tersebut didalam folder. Sama halnya dengan file unknown, yaitu file yang tidak memiliki format (ketika dalam nama file tidak ditemukan karakter `.`). file tersebut akan dipindahkan ke dalam folder Unknown. terakhir untuk file file lainnya akan dikategorikan berdasarkan formatnya menggunakan fungsi `getformat`

```c
char *getformat(char *filename) 
{
    char *ext = strchr(filename, '.');
    if(!ext || ext == filename)
    {
        return NULL;
    }
    return ext + 1;
}
``` 
disini kita mencari format file dengan mencari karakter `.` dan mengembalikan char-char setelahnya

setelah mendapat format filenya kita kembali pada fungsi `categorize` untuk kemudian ke fungsi `lowercase` 
```c
void lowercase(char s[]) 
{
  for(int i = 0; s[i] != '\0'; i++) 
  {
    s[i] = tolower(s[i]);
  }
}
```
disini kita mengubah nama file yang mempunyai huruf kapital menjadi lowercase untuk dijadikan folder kategori masing masing. 

setelah selesai kita kembali ke `categorize` untuk memindahkan file file sisanya ke folder yang sudah dibuat sesuai dengan formatnya. 

![image](/uploads/b663fe3685f1930b045116f3adc4ebef/image.png)

### 3.d,3.e
d. Untuk mengirimkan file ke Cocoyasi Village, nami menggunakan program client-server. Saat program client dijalankan, maka folder /home/[user]/shift3/hartakarun/” akan di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client.
e. Client dapat mengirimkan file “hartakarun.zip” ke server dengan mengirimkan command berikut ke server
```
send hartakarun.zip
```

sebelum kita mengirim file kita akan lakukan zip pada file file yang sudah dikategorikan menggunakan 
```c
system("zip hartakarun.zip -r /home/helmitaqiyudin/shift3/hartakarun");
```

selanjutnya kita mencari command `send` yang akan digunakan untuk mengirim file ke server
```c
if(argc != 3){
        perror("Invalid number of arguments\n");
        return -1;
    }
    if(strcmp(argv[1],"send") != 0){
        perror("Invalid command\n");
        return -1;
    }
    if(access(argv[2], F_OK) == -1){
        perror("File not found\n");
        return -1;
	}
```

pada saat dijalankan kita gunakan
```
./client send hartakarun.zip
```
untuk mengirim file ke server

![image](/uploads/86877dbc01ac198046d75defa8a301ed/image.png)
![image](/uploads/5dd745ea711ebf2b8b3340f89fb4669e/image.png)


### Permasalahan yang dihadapi
1. Stuck di 3a, yang proses unzip tanpa menggunakan exec dan system() jadi waktunya habis
2. lama memahami yang socket
3. waktu praktikum banyak dimakan sama tugas & uts


