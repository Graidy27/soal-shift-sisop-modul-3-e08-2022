#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <dirent.h>

pthread_t tid[10000000];
int pthreadcount = 0;
char dest[200] = "/home/helmitaqiyudin/shift3/hartakarun";

typedef struct arg_struct {
    char arg1[100];
    char arg2[100];
}args;

char *getformat(char *filename);
void lowercase(char s[]);
void listfilesrecursively(const char *name,int indent);
void *categorize(void *arg);
void join_threads();


int main(int argc, char **argv)
{
    chdir(dest);
    listfilesrecursively(".",0);
    join_threads();
}

char *getformat(char *filename) 
{
    char *ext = strchr(filename, '.');
    if(!ext || ext == filename)
    {
        return NULL;
    }
    return ext + 1;
}

void lowercase(char s[]) 
{
  for(int i = 0; s[i] != '\0'; i++) 
  {
    s[i] = tolower(s[i]);
  }
}

void listfilesrecursively(const char *name,int indent)
{
    DIR *dir;
    struct dirent *entry;
    int err;
    if (!(dir = opendir(name)))
    {
        printf("Cannot open directory '%s'\n", name);
        return;
    }
    while ((entry = readdir(dir)) != NULL) 
    {
        char path[1024];
        if (entry->d_type == DT_DIR) 
        {
            args *arg = malloc(sizeof(args));
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            listfilesrecursively(path, indent + 2); 
        }
        else
        {
            char cwd[100];
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            args *arg = malloc(sizeof(args));
            strcpy(arg->arg1,path);
            strcpy(arg->arg2,entry->d_name);
            err = pthread_create(&tid[pthreadcount++], NULL, &categorize, (void *)arg);
            if(err)
            {
                printf("\ncan't create thread :[%s]",strerror(err));
                continue;
            }
        }
    }
    closedir(dir);
}



void *categorize(void *arg)
{
    args *a = (args *)arg;
    char *path = a->arg1;
    char *src= a->arg2;
    char temp[200];
    char lowerext[100]; 

    if(src[0] == '.')
    {
        mkdir("Hidden",0777);
        sprintf(temp,"Hidden/%s",src);
        rename(path,temp);
    }
    else if(!getformat(src))
    {
        mkdir("Unknown",0777);
        sprintf(temp,"Unknown/%s",src);
        rename(path,temp);
    }
    else
    {
        char lowerext[50];
        strcpy(lowerext,getformat(src));
        lowercase(lowerext);
        mkdir(lowerext,0777);
        sprintf(temp,"%s/%s",lowerext,src);
        rename(path,temp);
    }
    memset(lowerext,0,sizeof(lowerext));
    memset(temp,0,sizeof(temp));
}

void join_threads()
{
    for(int i = 0; i < pthreadcount; i++)
    {
        pthread_join(tid[i], NULL);
    }
}
