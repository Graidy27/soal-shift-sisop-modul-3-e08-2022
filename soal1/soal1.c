#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <syslog.h>
#include <pwd.h>
#include <time.h>
#include <stdbool.h>
#include <dirent.h> 

char default_path[1024] = "/home/januarevan/modul3";
char *linkdatabase[] = {"https://drive.google.com/u/0/uc?id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1&export=download", "https://drive.google.com/u/0/uc?id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt&export=download"};
char *name[] = {"music.zip", "quote.zip", "hasil.zip"};
char *namefolder[] = {"music", "quote", "hasil"};
pthread_t tid[10];

char musicname[64][64];
char quotename[64][64];

int k1 = 0, k2 = 0;

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};
static char *decoding_table = NULL;
static int mod_table[] = {0, 2, 1};
 
void build_decoding_table() {
 
    decoding_table = malloc(256);
 
    for (int i = 0; i < 64; i++)
        decoding_table[(unsigned char) encoding_table[i]] = i;
}
 
 
void base64_cleanup() {
    free(decoding_table);
} 
 
char *base64_encode(const unsigned char *data,
                    size_t input_length,
                    size_t *output_length) {
 
    *output_length = 4 * ((input_length + 2) / 3);
 
    char *encoded_data = malloc(sizeof(char)*32768);
    if (encoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        u_int32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
        u_int32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
        u_int32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;
 
        u_int32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;
 
        encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
    }
 
    for (int i = 0; i < mod_table[input_length % 3]; i++)
        encoded_data[*output_length - 1 - i] = '=';
 
    return encoded_data;
}
 
 
unsigned char *base64_decode(const char *data,
                             size_t input_length,
                             size_t *output_length) {
 
    if (decoding_table == NULL) build_decoding_table();
 
    if (input_length % 4 != 0) return NULL;
 
    *output_length = input_length / 4 * 3;
    if (data[input_length - 1] == '=') (*output_length)--;
    if (data[input_length - 2] == '=') (*output_length)--;
 
    unsigned char *decoded_data = malloc(sizeof(char)*32768);
    if (decoded_data == NULL) return NULL;
 
    for (int i = 0, j = 0; i < input_length;) {
 
        u_int32_t sextet_a = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        u_int32_t sextet_b = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        u_int32_t sextet_c = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
        u_int32_t sextet_d = data[i] == '=' ? 0 & i++ : decoding_table[data[i++]];
 
        u_int32_t triple = (sextet_a << 3 * 6)
        + (sextet_b << 2 * 6)
        + (sextet_c << 1 * 6)
        + (sextet_d << 0 * 6);
 
        if (j < *output_length) decoded_data[j++] = (triple >> 2 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 1 * 8) & 0xFF;
        if (j < *output_length) decoded_data[j++] = (triple >> 0 * 8) & 0xFF;
    }
 
    return decoded_data;
}

void *downloadfile(void *arg){
    pid_t f1, f2;
    pthread_t id = pthread_self();
    if(pthread_equal(id,tid[0])){
        f1 = fork();
        if(f1 == 0){
            pid_t f3;
            f3 = fork();
            int status_f3;
            if(f3 == 0){
                char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[0], "-O", name[0], NULL};
                execv("/usr/bin/wget", argv);
            }
            else{
                while((wait(&status_f3)) > 0);
                char *argv[] = {"unzip", "-qq", name[0], "-d", namefolder[0], NULL};
                execv("/usr/bin/unzip", argv);
            }
        }
    }
    else if(pthread_equal(id,tid[1])){
        f2 = fork();
        if(f2 == 0){
            pid_t f4 = fork();
            int status_f4;
            if(f4 == 0){
                char *argv[] = {"wget", "-q", "--no-check-certificate", linkdatabase[1], "-O", name[1], NULL};
                execv("/usr/bin/wget", argv);
            }
            else{
                while((wait(&status_f4)) > 0);
                char *argv[] = {"unzip", "-qq", name[1], "-d", namefolder[1], NULL};
                execv("/usr/bin/unzip", argv);
            }
        }
    }
    else if(pthread_equal(id,tid[2])){
        pid_t f5 = fork();
        if(f5 == 0){
            pid_t f6 = fork();
            int status_f6;
            if(f6 == 0){
                char *argv[] = {"mkdir", "-p",  namefolder[0], NULL};
                execv("/usr/bin/mkdir", argv);
            }
            else{
                while((wait(&status_f6)) > 0);
                pid_t f7 = fork();
                int status_f7;
                if(f7 == 0){
                    char *argv[] = {"mkdir", "-p",  namefolder[1], NULL};
                    execv("/usr/bin/mkdir", argv);
                }
                else{
                    while((wait(&status_f7)) > 0);
                    char *argv[] = {"mkdir", "-p",  namefolder[2], NULL};
                    execv("/usr/bin/mkdir", argv);
                }
            }
        }
    }
}

void base64decode(){
    
}

void *decode(void *arg){
    pid_t f1, f2;
    pthread_t id = pthread_self();
    if(pthread_equal(id,tid[0])){
        DIR *d1;
        struct dirent *dir1;
        d1 = opendir("/home/januarevan/modul3/music");
        while ((dir1 = readdir(d1)) != NULL) {
            if(strstr(dir1->d_name, ".txt") != NULL){
                strcpy(musicname[k1], dir1->d_name);
                k1++;
            }
        }
        closedir(d1);
        char tempdir[FILENAME_MAX];
        char buffer[1024]; 
        FILE *fp2 = fopen("/home/januarevan/modul3/hasil/music.txt", "a");
        for(int i = 0; i < k2; i++){
            memset(buffer, 0, sizeof(buffer));
            strcpy(tempdir, default_path);
            strcat(tempdir, "/music/");
            strcat(tempdir, musicname[i]);
            FILE *fp1 = fopen(tempdir, "r");
            fgets(buffer, 1024, fp1);
            size_t decode_size = strlen(buffer);
            char *decoded = malloc(sizeof(char)*32768);
            decoded = base64_decode(buffer, decode_size, &decode_size);
            fprintf(fp2, "%s\n", decoded);
            fclose(fp1);
        }
        fclose(fp2);
    }
    else if(pthread_equal(id,tid[1])){
        DIR *d2;
        struct dirent *dir2;
        d2 = opendir("/home/januarevan/modul3/quote");
        while ((dir2 = readdir(d2)) != NULL) {
            if(strstr(dir2->d_name, ".txt") != NULL){
                strcpy(quotename[k2], dir2->d_name);
                k2++;
            }
        }
        closedir(d2);
        char tempdir[FILENAME_MAX];
        char buffer[1024]; 
        FILE *fp2 = fopen("/home/januarevan/modul3/hasil/quote.txt", "a");
        for(int i = 0; i < k2; i++){
            memset(buffer, 0, sizeof(buffer));
            strcpy(tempdir, default_path);
            strcat(tempdir, "/quote/");
            strcat(tempdir, quotename[i]);
            FILE *fp1 = fopen(tempdir, "r");
            fgets(buffer, 1024, fp1);
            size_t decode_size = strlen(buffer);
            char *decoded = malloc(sizeof(char)*32768);
            decoded = base64_decode(buffer, decode_size, &decode_size);
            fprintf(fp2, "%s\n", decoded);
            fclose(fp1);
        }
        fclose(fp2);
    }
}

void *zip(void *arg){
    pid_t f1, f2;
    pthread_t id = pthread_self();
    if(pthread_equal(id,tid[0])){
        f1 = fork();
        if(f1 == 0){
            char *argv[]={"zip", "-P", "mihinomenestjanuarevan", "-r", "hasil.zip", "hasil", NULL};
            execv("/usr/bin/zip",argv);
        }
    }
}

void *unzipadd(void *arg){
    pid_t f1, f2;
    pthread_t id = pthread_self();
    if(pthread_equal(id,tid[0])){
        f1 = fork();
        if(f1 == 0){
            char *argv[]={"rm","-r","hasil", NULL};
            execv("/usr/bin/rm",argv);
        }
    }
    else if(pthread_equal(id,tid[1])){
        f2 = fork();
        if(f2 == 0){
            usleep(1000);
            char *argv[] = {"unzip", "-P", "mihinomenestjanuarevan", "-qq", name[2], NULL};
            execv("/usr/bin/unzip", argv);
        }
    }
    else if(pthread_equal(id,tid[2])){
        FILE *fp1 = fopen("/home/januarevan/modul3/no.txt", "a");
        fprintf(fp1,"No");
        fclose(fp1);
    }
}

void *zip2(void *arg){
    pid_t f1, f2;
    pthread_t id = pthread_self();
    if(pthread_equal(id,tid[0])){
        f1 = fork();
        if(f1 == 0){
            char *argv[]={"zip","-P","mihinomenestjanuarevan", "-r", "hasil.zip", "hasil", "no.txt", NULL};
            execv("/usr/bin/zip",argv);
        }
    }
    else if(pthread_equal(id,tid[0])){
        f2 = fork();
        if(f2 == 0){
            char *argv[]={"rm","-r","hasil.zip", NULL};
            execv("/usr/bin/rm",argv);
        }
    }
}


int main(){
    int err;
    for(int i = 0; i < 3; i++){
        err = pthread_create(&(tid[i]), NULL, &downloadfile, NULL);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);

    sleep(5);
    for(int i = 0; i < 2; i++){
        err = pthread_create(&(tid[i]), NULL, &decode, NULL);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    sleep(1);
    for(int i = 0; i < 1; i++){
        err = pthread_create(&(tid[i]), NULL, &zip, NULL);
    }
    pthread_join(tid[0], NULL);
    sleep(1);
    for(int i = 0; i < 3; i++){
        err = pthread_create(&(tid[i]), NULL, &unzipadd, NULL);
    }
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
    pthread_join(tid[2], NULL);
    sleep(5);
    for(int i = 0; i < 1; i++){
        err = pthread_create(&(tid[i]), NULL, &zip2, NULL);
    }
    pthread_join(tid[0], NULL);
    exit(0);
    return 0;
}
