#include <sys/stat.h>	//Buat mkdir
#include <pthread.h>	//Buat thread
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#define PORT 8080
char path[] = "/home/graidy/Sisop/soal-shift-sisop-modul-3-e08-2022/soal2/";
int total_user = 0;
char login_user[1024] = {0};
char tsv_path[1024] = {0};

//NOMOR 2a
int dua_a(int new_socket){
	write(new_socket, "Go", 1024);
	char option[2] = {0}, username[1024] = {0}, password[1024] = {0};
	read(new_socket, option, 2); 		//puts(option);
	read(new_socket, username, 1024);	//puts(username);
	read(new_socket, password, 1024);	//puts(password);
	FILE* fp;
	char temp_path[1024] = {0};
	strcat(temp_path, path);
	strcat(temp_path, "/Server/users.txt");
	fp = fopen(temp_path, "a+");
	char temp[1024] = {0}, db[1024] = {0}, login[2] = {0};
	int correct_pass = 0, user_uniq = 1;
	strcat(temp, username);
	strcat(temp, ":");
	strcat(temp, password);
	while(fgets(db, sizeof(db), fp)){
		total_user++;
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';
		if(option[0] == '0') {
			if(!strcmp(temp, db)) {
				correct_pass = 1;
				break;
			}
			continue;
		}
		int i = 0;
		while(i < strlen(temp) && i < strlen(db)){
			if(db[i] != temp[i]) break;
			if(db[i] == ':') user_uniq = 0;
			i++;
		}
		if(user_uniq == 0) break;
	}
	char status[200] = {0};
	if(option[0] == '0') {
//		printf("Login\n");
		if(correct_pass) {
			strcpy(status, "ACCESS GRANTED");
			strcpy(login_user, username);
		}
		else strcpy(status, "ACCESS DENIED");
	}
	else if(option[0] == '1') {
//		printf("Register\n");
		if(!user_uniq) strcpy(status, "USERNAME EXIST");
		else if(user_uniq){
			if(total_user >0) fprintf(fp, "\n");
			fprintf(fp, "%s", temp);
			strcpy(status, "USERNAME REGISTERED");
		}
	}
	write(new_socket, status, 200);
	char back_to_main_menu[2] = {0};
	read(new_socket, back_to_main_menu, 2);
//	printf("%c\n", back_to_main_menu[0]);
	fclose(fp);
	if(strcmp(status, "ACCESS GRANTED") == 0) return 1;
	if(back_to_main_menu[0] == 'Y') return dua_a(new_socket);
	return 0;
}

//Nomor 2b
void dua_b(){
	//Arraynya harus dikosongin dulu, kalo gak core dump
	//Kenapa? Soalnya make strcat 2x. Jadi di tambahkannya di index 200++
	strcpy(tsv_path, path);				//Ini cat pertama
	strcat(tsv_path, "/Server/problem.tsv");	//Ini cat kedua
	//Kalo gak mau kena core dumb di array yang gak kosong, str cat yang awal ganti jadi scrcpy aja:
	//char temp_path[200];
	//strcpy(temp_path, path);
	//strcat(temp_path, "/Server/problem.tsv");

	FILE* fp;
	if(fp = fopen(tsv_path, "r")) {printf("Exist\n");fclose(fp);}
	else{
		printf("File doesn't exist\n");
		fp = fopen(tsv_path, "a");
		fprintf(fp, "Judul Problem\tAuthor Problem\n");
		fclose(fp);
	}
}


//NOMOR 2c
void take_mid_only(char *a){
	char temp[100] = {0};
	int i = 1;
	while(*(a + i) != '>'){
		temp[i-1] = *(a + i);
		i++;
	}
	strcpy(a, temp);
}

void copy_file(char *tujuan, char *path_asal, char file_type[100]){
	FILE *fp_asal = fopen(path_asal, "r");

	char path_tujuan[1024] = {0};
	strcpy(path_tujuan, tujuan);
	strcat(path_tujuan, file_type);
	FILE *fp_tujuan = fopen(path_tujuan, "a");
	char temp[1024] = {0};
//	printf("tujuan : %s\nasal: %s\n\n", path_tujuan, path_asal);
	while(fgets(temp, sizeof(temp), fp_asal)) fprintf(fp_tujuan, "%s", temp);
	fclose(fp_asal);
	fclose(fp_tujuan);
}

void dua_c(int new_socket){
	//Notice
	write(new_socket, "\nIf the problem name already exist, the program will stop immediately.\nProblem name, input, and output will\nbe updated after user exit the program\n\n", 1024);
	char a[1024] = {0}, apn[1024] = {0}, temp_path[1024] = {0};

	write(new_socket, "Judul problem: ", 1024); 		read(new_socket, a, 1024);
	take_mid_only(a); //puts(a);
	//folder
	if(mkdir(a, S_IRWXU)) {write(new_socket, "\0", 1024); return;}

	//upate tsv
	FILE *fp = fopen(tsv_path, "a");
	fprintf(fp, "%s\t%s\n", a, login_user);
	fclose(fp);


	strcpy(temp_path, path);
	strcat(temp_path, "Server/");
	strcat(temp_path, a);

	//description
	write(new_socket, "Filepath description.txt: ", 1024);	read(new_socket, a, 1024); //puts(a);
	take_mid_only(a); //puts(a);
	copy_file(temp_path, a, "/description.txt");

	//input
	write(new_socket, "Filepath input.txt: ", 1024);	read(new_socket, a, 1024); //puts(a);
	take_mid_only(a); //puts(a);
	copy_file(temp_path, a, "/input.txt");

	//output
	write(new_socket, "Filepath output.txt: ", 1024);	read(new_socket, a, 1024); //puts(a);
	take_mid_only(a); //puts(a);
	copy_file(temp_path, a, "/output.txt");

	//stop input
	write(new_socket, "\0", 1024);
}

//NOMOR 2d
void dua_d(int new_socket){
//	printf("2d\n");
        FILE* fp; int judul = 1;
	if(fp = fopen(tsv_path, "r")) {
	//	printf("Ada ges\n");
		char temp[1024] = {0};
		//mengirim semua data soal
		while(fgets(temp, sizeof(temp), fp)) {
			if(judul) {judul = 0; continue;}
			char temp2[1024] = {0}; int i = -1, j = 0;
			while(1){
				i++;
				if(temp[i] == '\t') {
					temp2[j] = '\0';
//					printf("judul: %s\n", temp2);
					write(new_socket, temp2, 1024);
					j = 0;
					continue;
				}
				else if(temp[i] == '\n'){
					temp2[j] = '\0';
					//printf("author: %s\n", temp2);
					write(new_socket, temp2, 1024);
					break;
				}
				temp2[j] = temp[i];
				j++;
			}
		}
		write(new_socket, "\0", 10);
		fclose(fp);
 	}
}

//NOMOR 2e
void dua_e(int new_socket){
	char message[1024] = {0};
	read(new_socket, message, 1024);
	take_mid_only(message);
	if(mkdir(message, S_IRWXU)) {
		write(new_socket, message, 1024);
		char c_working_dir[1024] = {0}, problem_dir[1024] = {0};
		getcwd(problem_dir, 1024);
		strcat(problem_dir, "/");
		strcat(problem_dir, message);
		strcat(problem_dir, "/");

		int cd = strlen(problem_dir);
		read(new_socket, c_working_dir, 1024);
		strcat(c_working_dir, "/");
		strcat(c_working_dir, message);

		//description
		strcat(problem_dir, "description.txt");
		copy_file(c_working_dir, problem_dir, "/description.txt");
		problem_dir[cd] = '\0';

		//input
		strcat(problem_dir, "input.txt");
		copy_file(c_working_dir, problem_dir, "/input.txt");
		problem_dir[cd] = '\0';

		//output
		strcat(problem_dir, "output.txt");
		copy_file(c_working_dir, problem_dir, "/output.txt");

		write(new_socket, "Problem name, input, and output will\nbe updated after user exit the program\n\n", 1024);
	}
	else {rmdir(message); write(new_socket, "\0", 1024);}
}

//NOMOR 2f
void dua_f(int new_socket){
	char judul[1024] = {0}, ans_path[1024] = {0};
	read(new_socket, judul, 1024); take_mid_only(judul);
	read(new_socket, ans_path, 1024); take_mid_only(ans_path);
//	printf("judul: %s\nans_path: %s\n", judul, ans_path);
	if(mkdir(judul, S_IRWXU)) {
		//set status
		write(new_socket, "Problem found\n", 1024);
		char problem_dir[1024] = {0};
		getcwd(problem_dir, 1024);
		strcat(problem_dir, "/");
		strcat(problem_dir, judul);
		strcat(problem_dir, "/output.txt");
//		printf("ans: %s\nprob: %s\n", ans_path, problem_dir);
		FILE *fp_prob = fopen(problem_dir, "r");
		FILE *fp_ans = fopen(ans_path, "r");
		char temp[1024] = {0}, temp2[1024] = {0};
		int a = 1, p = 1, same = 0;

		while(a == 1 || p == 1) {
			if(strcmp(temp, temp2)) break;
			if(!fgets(temp, sizeof(temp), fp_prob)) p = 0;
			if(!fgets(temp2, sizeof(temp2), fp_ans)) a = 0;
			if(a == 0 && p == 0) same = 1;
		}
		if(same) write(new_socket, "AC", 1024);
		else write(new_socket, "WA", 1024);
		fclose(fp_ans);
		fclose(fp_prob);
		write(new_socket, "\0", 1024);
	}
	else {rmdir(judul); write(new_socket, "\0", 1024);}
}

//NOMOR 2g
void *connection_control(void *new_sock){
	int new_socket = *(int*) new_sock;
	dua_b();
	if(dua_a(new_socket)) {
		printf("Client Login\n");
		while(1){
			char com[1024] = {0};
			read(new_socket, com, 1024);
//			printf("masok while\n");
			if(!strcmp(com, "add")) dua_c(new_socket);
			else if(!strcmp(com, "see")) dua_d(new_socket);
			else if(!strcmp(com, "download")) dua_e(new_socket);
			else if(!strcmp(com, "submit")) dua_f(new_socket);
			else if(!strcmp(com, "exit")) {printf("See you later!\n"); break;}
		}
	}
}


int main(int argc, char const *argv[]) {
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1;
	int addrlen = sizeof(address);

	//Create socket
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}


	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	//Prepare the sockaddr_in structure
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	//Bind
	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	//Listen
	if (listen(server_fd, 1) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	//Accept connection from client
	puts("Waiting for client...");
	
	//NOMOR 2g
	while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))) {
		printf("Connected to client\n");
		pthread_t client_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		if(pthread_create(&client_thread, NULL, connection_control, (void*) new_sock) < 0) {
			perror("Could not create thread");
			return 1;
		}
		pthread_join(client_thread, NULL);
		puts("Disconnected");
		puts("Waiting for client");
	}
	if(new_socket < 0){
		perror("accept");
		exit(EXIT_FAILURE);
	}




//	valread = read( new_socket , buffer, 1024);
//	printf("%s\n",buffer );
//	send(new_socket , hello , strlen(hello) , 0 );
//	printf("Hello message sent\n");
	return 0;
}
