#include <sys/stat.h>   //Buat mkdir
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080
int kecil = 0, besar = 0, angka = 0, invalid = 0;

//NOMOR 2a
void in(char *var){
	int i = 0;
	while(true){
		scanf("%c", &*(var+i));
		if(*(var+i) <= 90 && *(var+i) >= 65) {
			i++;
			besar = 1;
		}
		else if(*(var+i) <= 122 && *(var+i) >= 97) {
			i++;
			kecil = 1;
		}
		else if(*(var+i) <= 57 && *(var+i) >= 48) {
			i++;
			angka = 1;
		}
		else if(*(var+i) == '\n' && i != 0) {
			*(var+i) = '\0';
			return;
		}
		else if(*(var+i) == '\n' && i == 0) continue;
		else invalid = 1;
	}
}

int dua_a(int sock){
	puts("You will access the server when no one is currently accessing it\nPlease wait...\n");
	sleep(3);
	char permission[1024] = {0};
	read(sock, permission, 1024);
	puts("Welcome!"); sleep(2);
	printf("Please input 0 or 1\n");
	printf("0 : Login\n1 : Register\n");
	char option[2]; in(option);
	write(sock, option, 2);

	char username[1024], password[1024];
	while(true){
		invalid = 0;
		printf("Username : ");
		in(username);
		if(besar == 0 && kecil == 0) {printf("Username must cointain either uppercase or lowercase\n"); continue;}
		if(!invalid) {write(sock, username, 1024); break;}
		else {printf("Invalid character\n"); continue;}
	}

	while(true){
		invalid = 0; besar = 0; kecil = 0; angka = 0;
		printf("Password : ");
		in(password);
		if(besar == 0 || kecil == 0 || angka == 0) {printf("Please use upper case, lower case, AND number\n"); continue;}
		if(!invalid) {
			if(strlen(password) > 5) {write(sock, password, 1024); break;}
			else printf("Must contain at least 6 character\n");
		}
		else printf("Invalid character\n");
	}
	char status[200] = {0};
	read(sock, status, 200);
	printf("%s\n", status);
	if(strcmp(status, "USERNAME EXIST") == 0 || strcmp(status, "USERNAME REGISTERED\0") == 0 || strcmp(status, "ACCESS DENIED") == 0) {
		printf("Back to main menu? (Y/N)\n");
		char c[2]; scanf("%c", &c[0]);
		while(c[0] != 'Y' && c[0] != 'N') {if(c[0] != '\n') printf("Wrong Input. Try again\n"); scanf("%c", &c[0]);}
		write(sock, c, 2);
		if(c[0] == 'Y') return dua_a(sock);
		return 0;
	}
	return 1;
}

//NOMOR 2c
void dua_c(int sock){
	//printf("dua c\n");
	char q[1024] = {0}, a[1024] = {0};
	read(sock, q, 1024); printf("%s", q);
	read(sock, q, 1024);
	while(strlen(q)) {
		printf("%s", q);
		scanf("%s", a);
		write(sock, a, 1024);
		read(sock, q, 1024);
	}
}

//NOMOR 2d
void dua_d(int sock){
//	printf("dua d\n");
	char message[1024] = {0};

	int spasi = 1, i = 2;
	read(sock, message, 1024);
//	printf("T: %s\n", message);
	while(strlen(message)){
		printf("<%s>", message);
		if(spasi) {printf(" "); spasi = 0;}
		else {printf("\n"); spasi = 1;}
		read(sock, message, 1024);
	}
	puts("");
}

//NOMOR 2e
void dua_e(int sock){
	char message[1024] = {0}; scanf("%s", message);
	write(sock, message, 1024);
	read(sock, message, 1024);

	//check problem availability
	if(!strlen(message)) {printf("Problem name not availabel\n\n"); return;}

	//mkdir
	mkdir(message, S_IRWXU);

	//send current working directory
	getcwd(message, 1024); write(sock, message, 1024);

	//get notice
	read(sock, message, 1024);
	printf("%s", message);
	sleep(2);
}

//NOMOR 2f
void dua_f(int sock){
	char judul[1024] = {0}, ans_path[1024] = {0};
	scanf("%s%s", judul, ans_path);
	write(sock, judul, 1024);
	write(sock, ans_path, 1024);

	char status[1024] = {0};
	read(sock, status, 1024);

	//check problem availability
	if(!strlen(status)) {printf("Problem name not availabel\n\n"); return;}

	//check status
	while(strlen(status)) {
		printf("%s\n", status); sleep(2);
		read(sock, status, 1024);
	}
}

//dua_g di server

int main(int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	printf("Request a connection to server...");
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	}
	else {
//		dua_b di server.c
		puts("Connection established\n");
		if(dua_a(sock) == 1) {
			printf("Welcome!");
			write(sock, "add", strlen("add"));
			while(true){
				puts("\nPlease select the command below");
				printf("- add\n- see\n- download <judul-problem>\n- submit <judul-problem> <path-file-output>\n- exit\n");
				char com[1024] = {0}; scanf("%s", com);
				write(sock, com, 1024);
				if(!strcmp(com, "add")) dua_c(sock);
				else if(!strcmp(com, "see")) dua_d(sock);
				else if(!strcmp(com, "download")) dua_e(sock);
				else if(!strcmp(com, "submit")) dua_f(sock);
				else if(!strcmp(com, "exit")) {printf("See you later!\n"); break;}
				else {printf("Wrong input\n"); continue;}
			}
		}
		else printf("See you later!\n");
	}


//	send(sock , hello , strlen(hello) , 0 );
//	printf("Hello message sent\n");
//	valread = read( sock , buffer, 1024);
//	printf("%s\n",buffer );
	return 0;
}

